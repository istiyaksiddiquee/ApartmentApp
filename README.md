# Apartment Searching App 

This app is dedicated for searching apartments for rent based on location and other preferences. This app takes the help of google maps for location based services. NodeJS is used in back end and Bootstrap and jQuery on front. MongoDB is used as data storage. Both front and back is Dockerized, Docker-Compose is used to bring up the containers and network between them. the only open port is 80, which is mapped to native 80 port, so it can be reached using http://localhost. 

In order to bring up the app, just use up.bat and to tear down everything use down.bat, that's all you need to do to run this app.

Thank you. 

P.S: install the following plugin in chrome: https://goo.gl/XMJiWd